export default {
  Username: 'Username',
  Password: 'Password',
  Load: 'Load',
  EU: 'Europe',
  NA: 'North America',
  AP: 'Asia Pacific',
  KO: 'Korea',
  Jan: 'Jan',
  Feb: 'Feb',
  Mar: 'Mar',
  Apr: 'Apr',
  May: 'May',
  Jun: 'Jun',
  Jul: 'Jul',
  Aug: 'Aug',
  Sep: 'Sep',
  Oct: 'Oct',
  Nov: 'Nov',
  Dec: 'Dec',
  Contact: 'Contact me!',
  Unrunked: 'Unrunked',
  Iron: 'Iron',
  Bronze: 'Bronze',
  Silver: 'Silver',
  Gold: 'Gold',
  Platinum: 'Platinum',
  Diamond: 'Diamond',
  Immortal: 'Immortal',
  Radiant: 'Radiant',
  Searching: 'Searching for matches...',
  LoginError: 'Something went wrong with the login, check your credentials and try again!',
  AccountInformationError: 'We couldn\'t retrive the required information for your account, sorry...',
  UnknownRankedError: 'We couldn\'t retrive any valid match from your account, make sure to have played a ranked in the past 40 games',
  SupportMessage: 'This is a completly free and safe to use service, if you like it support the costs of the servers!',
  SupportButton: 'Support the project',
  NoRankedFound: 'We couldn\'t find any valid ranked on your account',
  MistakeMsg: 'If you think that this is a mistake',
  TooManyRequests: 'You are sending too many request wait some seconds!',
  SavePassword: 'Do you want to save your password for 24 hours?',
  SavedInfo: 'It will be saved only on your browser',
  Yes: 'Yes',
  No: 'No',
  Yes30: 'Yes, for 30 days',
  footer1: 'This site is not related to Riot, we use only api obtained from',
  footer2: 'and we don\'t storage any sensitive data.',
  footer3: 'We use Cookies only for saving your password and username on your current session (not on our server) for',
  hours: 'hours',
  footer4: 'The source codes are live updated through pipelines',
  Warning: 'Site offline!',
  Info: ''
}
