import enUS from './en-us'
import itIT from './it-it'
import esEs from './es-es'
import deDe from './de-de'
import trTr from './tr-tr'
import arAr from './ar-ar'

export default {
  'en-us': enUS,
  'it-it': itIT,
  'es-es': esEs,
  'de-de': deDe,
  'tr-tr': trTr,
  'ar-ar': arAr
}
